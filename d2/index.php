<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>WD007 - PHP + MySQL</title>
</head>

<body>
    <h2>PHP Structure</h2>
    <?php
    	/*=========================== 
        - By default, PHP documents end with the extension ___.php____
        - To trigger PHP commands, use the starting tag <?php and
        the closing tag ?>
        - <?php … ?> is called ___delimiter___
        - to output values, use the keyword ___ECHO_
        =============================*/
        echo "Hello, Philippines";
        echo "<p>Hello, World</p>";		/*add tag  < p >  to make it block element*/
        echo "<p>Hello, <strong>World</strong></p>";
    ?>


	<h2>Variables</h2>
	<?php
		/*============================
		- Variables are used to store ____DATA____ and provide ____STORED____ data when needed.
		- in PHP, all variable names must start with the ___$___ sign or ("sigul");
		=============================*/
		$name = "Karen";
		echo "My name is $name";
		echo '<p>My name is $name</p>';		/*single quote - return the variable name as is*/

		// - variables are case-sensitive
		// echo $Name;							//error because variable has small 'n'

		// - variable names contain only a-z, A-Z, 0-9, and _ 
		// - variables start with a letter or _
		$_description = "I'm sexy and I know it.";
		echo "<p>My name is $name. $_description</p>";

		// - variable names must not contain any spaces
		$first_name = "Test";
		echo $first_name;

		// - variable names must not contain - signs
		

		// - use underscore in place of the space
		
	?>
	<h2>Data Types</h2>
	
	<?php 
		/* ===========================
		- PHP is a __LOOSELY___ typed language; it does not have ___EXPLICITLY DEFINED___ data types. 
		- It determines the data types by analyzing the attributes of data supplied. 
		- It PHP implicitly supports the following data types: 
		1) Integers
		2) Floating Point Numbers
		3) String
		4) Boolean
		=============================
   		1) INTEGERS
   		- whole numbers e.g. -3, 0, 69. 
   		- The maximum value of an integer is _____PLATFORM-DEPENDENT______. 
   		- On a 32 bit machine, it’s usually around 2 billion. 64 bit machines usually have larger values. 
   		=============================*/
   		$student_num = 2940;
   		echo "<div>$student_num</div>";


	    /*=============================
   		2) FLOATING POINT NUMBERS
   		- decimal numbers
   		- aka double or real numbers.  
   		- The maximum value of a float is _____PLATFORM-DEPENDENT_____. 
   		- are larger than integers.
   		=============================*/
   		$max_speed = 104.87;
   		echo "<div>$max_speed</div>";


    	/*=============================
   		3) STRING
   		- a sequence of characters
   		- enclosed in single or double quotes
   		- ___SINGLE____-quoted - preserves the exact contents of a string
   		- ___DOUBLE____-quoted - includes the value of a variable inside a string
   		=============================*/
   		$username = "John";
   		echo '<p>Hey, $username</p>';
   		echo "<p>Hey, $username</p>";


   		/*=============================
   		4) BOOLEAN
   		- true or false
   		=============================*/
   		$is_admin = true;
   		echo "<div>$is_admin</div>";
	?>

	<h2>Constants</h2>
	<?php
		/*=============================
		- A constant is a variable whose value cannot be changed at ___RUNTIME___.
		- By convention, constant identifiers are always ____UPPERCASE____
		- SYNTAX define("NAME","value");
		==============================*/
		define("PI", 3.14);
		echo "<p>PI</p>";
		echo '<p>PI</p>';
		echo "<p>" . PI . "</p>";			//for constant variable
	?>


	<h2>Arithmetic Operators</h2>
	<?php
		/*=============================
		- used to perform arithmetic operations on ____NUMERIC___ data. 
		- concatenate operator works on strings values too.
		1) Addition
		2) Subtraction
		3) Multiplication
		4) Division
		5) Modulus
		6) Increment
		7) Decrement
		8) Operator Precedence
		==============================*/
		$num1 = 3;
		$num2 = 4;
		//1) ADDITION
		$sum = $num1 + $num2;
		echo "<div>$sum</div>";
		
		//2) SUBTRACTION
		$difference = $num1 - $num2;
		echo "<div>$difference</div>";

		//3) MULTIPLICATION
		$product = $num1 * $num2;
		echo "<div>$product</div>";
		
		//4) DIVISION
		$quotient = $num1 / $num2;
		echo "<div>$quotient</div>";
		
		//5) MODULUS
		$remainder = $num1 % $num2;
		echo "<div>$remainder</div>";
		
    	//6) INCREMENT
    	echo "<div>num2 is $num2</div>";
    	echo "<div>Post Increment++: " . $num2++ . "</div>";
    	echo "<div>num2 is $num2</div>";

    	$num2 = 4;
    	echo "<div>num2 is $num2</div>";
    	echo "<div>Post ++Increment: " . ++$num2 . "</div>";

    	//7) DECREMENT
    	$num2 = 4;
    	echo "<div>num2 is $num2</div>";
    	echo "<div>Post Decrement--: " . $num2-- . "</div>";
    	echo "<div>num2 is $num2</div>";
    	//
    	$num2 = 4;
    	echo "<div>num2 is $num2</div>";
    	echo "<div>Post --Decrement: " . --$num2 . "</div>";
    	echo "<div>num2 is $num2</div>";
    	//

    	//8) OPERATOR Precedence		//PMDAS
    	echo 1 + 5 * 3;
    	echo "<br>";
    	echo (1+5)*3;
	?>

	<h2>Comparison Operators</h2>
	<?php
		/*====================
		- used to compare values and data types

		1) Equality
		2) Strict Equality - compares __BOTH__ values and data types
		3) Not Equal
		4) Greater Than
		5) Less Than
		6) Greater Than or Equal
		7) Less Than or Equal
		======================*/
		//1) EQUALITY
	   	echo 1 == "1";		//1 (true) NOT strict equality (==) | different data types
	   	echo "<br>";
	   	echo true == 1;
	   	echo "<br>";

	    //2) STRICT EQUALITY
	   	echo 1 === "1";		//__ - blank (false) STRICT equality (===)
	   	echo "<br>";

	   	echo 1 === 1;		//1 (true) (false) STRICT equality (===)
	   	echo "<br>";
	   	echo true === 1;
	   	echo "<br>";

	    //3) NOT EQUAL
	   	echo 2 != 1;		//1 (true)
	   	echo "<br>";

	    //4) GREATER THAN
	    echo 3 > 1;
	    echo "<br>";

	    //5) LESS THAN
	    echo 3 < 1;
	    echo "<br>";

	    //6) GREATER THAN OR EQUAL
	    echo 1 >= 1;
	    echo "<br>";

	    //7) LESS THAN OR EQUAL
	    echo 8 <= 6;
	    echo "<br>";
	?>

	<h2>Logical Operators</h2>
	<?php 
		/*====================
		- any number greater than or less than zero (0) evaluates to true.
		- Zero (0) evaluates to false;
		1) and
		2) or
		3) xor
		4) ! (not)
		5) && (and)
		6) || (or)
		=====================*/

		//1) AND - TRUE if both $a and $b are TRUE.
		 $a = TRUE;
		 $b = TRUE;
		 echo $a and $b;
		 echo "<br>";

		//2) OR - TRUE if either $a or $b is TRUE.
		 $a = TRUE;
		 $b = FALSE;
		 echo $a or $b;
		 echo "<br>";
		
		//3) XOR - TRUE if either $a or $b is TRUE; they should have different values.
		 $a = TRUE;
		 $b = FALSE;
		 echo $a xor $b;
		 echo "<br>";
		 $a = TRUE;
		 $b = TRUE;
		 echo $a xor $b;
		 echo "<br>";

		//4) NOT - TRUE if variable (e.g. $a) is not TRUE (reverse of current val).
		//reverses the value of your current variable
		 $a = FALSE;
		 echo !$a;	//results 1-true; reverses its current value which is false
		 echo "<br>";
			
		//5) && - TRUE if both $a and $b are TRUE.
		$a = TRUE;
		$b = TRUE;
		echo $a && $b;
		echo "<br>";

		//6) ||  - TRUE if either $a or $b is TRUE.
		 $a = TRUE;
		 $b = FALSE;
		 echo $a or $b;
		 echo "<br>";
	?>

	<h2>PHP Arrays</h2>
	<?php
		/*====================
		- special variable, which can hold more than one value at a time.
		- in PHP, there are 3 types of array:
		1) Indexed - with numeric ___INDEX___
		2) Associative - with named ___KEYS___
		3) Multidimensional - contains 1 or more ___ARRAYS___
		=====================*/

		//1) INDEXED ARRAY
		$topics = array("HTML", "CSS", "JS", "PHP");
		echo "<p>You are the ". $topics[1] ." of my ". $topics[0].".</p>";


		//Updating an array
		echo $topics[0];
		echo "<br>";
		$topics[0] = "HyperText Markup Language";
		echo $topics[0];


		//2) ASSOCIATIVE ARRAY
			//a) Creating Associative Arrays
				//i) first method
				$student_one = array(
					"Math" => 95,
					"Physics" => 90,
					"Chemistry" => 96,
					"Filipino" => 70
				);
  
				//ii) second method 
		  		$student_two["Math"] = 95;
		  		$student_two["Physics"] = 90;
		  		$student_two["Chemistry"] = 96;
		  		$student_two["Filipino"] = 70;

		  		//echo $student_one;	//error; user 'print_r' to display array
		  		echo "<br>";
		  		print_r($student_one);
		  		echo "<br>";
		  		print_r($student_two);
		  		echo "<br>";

		  		echo "<pre>";
		  		print_r($student_one);
		  		echo "<pre>";
		  		print_r($student_two);
		  		echo "<pre>";

			//b) Accessing the elements directly
			echo "<hr>";
			echo "<div>Marks for student one are: </div>";
			echo "<div>Math: " . $student_one["Math"]. "</div>";
			echo "<div>Physics: " . $student_one["Physics"]. "</div>";
			echo "<div>Chemistry: " . $student_one["Chemistry"]. "</div>";
			echo "<div>Filipino: " . $student_one["Filipino"]. "</div>";
			echo "<hr>";


		//2) MULTIDIMENSIONAL ARRAY
			//a) Creating multidimensional array
			$my_array = array(
				array("Ankit", "Ram", "Shyam"),
				array("Unnao", "Trichy", "Kanpur")
			);

			//b) Displaying the array information
			echo "<pre>";
			print_r($my_array);
			echo "</pre>";

	?>

</body>

</html>