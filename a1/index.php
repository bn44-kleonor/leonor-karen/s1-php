<!DOCTYPE html>
<html>
<head>
	<title>WD007 - PHP + MySQL (activity 1)</title>

	<!-- META TAGS -->
	<meta charset="utf-8">
	<!-- DISABLE VIEWPORT ZOOM -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE-Edge">

	<!-- FONT AWESOME -->
	<script src="https://kit.fontawesome.com/ac74f518bc.js"></script>

	<!-- BOOTSTRAP CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!-- 	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap/bootstrap.min.css"></head> -->

	<!-- EXTERNAL CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>

<?php

/* ================
Instruction
1. Create a new folder inside s1 and name it as a1.
2. Inside a1, create the files index.php
3. Using multidimensional array, represent the information in the picture
================== */


	//a) Creating multidimensional array
	$movie = array(
		array(
				"Movie_Title" => "Pink Panther",
				"Category" => "Comedy"
			),
		array(
				"Movie_Title" => "Johnny English",
				"Category" => "Comedy"
			),
		array(
				"Movie_Title" => "Die Hard",
				"Category" => "Action"
			),
		array(
				"Movie_Title" => "Expendables",
				"Category" => "Action"
			),
		array(
				"Movie_Title" => "Exorcist",
				"Category" => "Horror"
			),
		array(
				"Movie_Title" => "Romeo and Juliet",
				"Category" => "Romance"
			),
		array(
				"Movie_Title" => "See No Evil Hear No Evil",
				"Category" => "Comedy"
			)
	);

	//b) Displaying the array information
	echo "<pre>";
	//print_r($movie);
	echo "</pre>";
	echo " ";
	echo " ";
    echo " ";

    //echo $movie[0]["Movie_Title"];
    echo "<br>";

    echo "<table>
            <thead>
                <tr>
                    <th>Movie Title</th>
                    <th>Category</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>" . $movie[0]["Movie_Title"] . "<td>
                    <td>" . $movie[0]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[1]["Movie_Title"] . "<td>
                    <td>" . $movie[1]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[2]["Movie_Title"] . "<td>
                    <td>" . $movie[2]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[3]["Movie_Title"] . "<td>
                    <td>" . $movie[3]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[4]["Movie_Title"] . "<td>
                    <td>" . $movie[4]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[5]["Movie_Title"] . "<td>
                    <td>" . $movie[5]["Category"] . "<td>
                </tr>
                <tr>
                    <td>" . $movie[6]["Movie_Title"] . "<td>
                    <td>" . $movie[6]["Category"] . "<td>
                </tr>
            </tbody>
        </table>";

echo "<br>";
echo "<br>";
echo "<br>";

// Using bootstrap
echo "
    <table class='table'>
    <thead class='thead-dark'>
        <tr>
        	<th scope='col'>Movie Title</th>
        	<th scope='col'>Category</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        	<td>" . $movie[0]['Movie_Title'] . "</td>
        	<td>" . $movie[0]['Category'] . "</td>
        </tr>
        <tr>
        	<td>" . $movie[1]['Movie_Title'] . "</td>
        	<td>" . $movie[1]['Category'] . "</td>
        </tr>
        <tr>
        	<td>" . $movie[2]['Movie_Title'] . "</td>
       		<td>" . $movie[2]['Category'] . "</td>
        </tr>
        <tr>
       		<td>" . $movie[3]['Movie_Title'] . "</td>
        	<td>" . $movie[3]['Category'] . "</td>
        </tr>
        <tr>
        	<td>" . $movie[4]['Movie_Title'] . "</td>
        	<td>" . $movie[4]['Category'] . "</td>
        </tr>
        <tr>
        	<td>" . $movie[5]['Movie_Title'] . "</td>
        	<td>" . $movie[5]['Category'] . "</td>
        </tr>
        <tr>
        	<td>" . $movie[6]['Movie_Title'] . "</td>
        	<td>" . $movie[6]['Category'] . "</td>
        </tr>
    </tbody>
    </table>
"

?>


</body>
</html>
